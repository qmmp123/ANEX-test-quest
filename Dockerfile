FROM golang

RUN go get -u github.com/go-gem/gem
RUN go get -u github.com/go-pg/pg
RUN go get -u github.com/go-gem/middleware-auth

RUN mkdir /code
WORKDIR /code
ADD . /code/

EXPOSE 8080