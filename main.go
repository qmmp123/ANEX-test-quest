package main

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"log"
	"math/rand"
	"strconv"

	"github.com/go-gem/gem"
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

type User struct {
	Id       int64
	Nickname string `json:"nickname"`
	Login    string `json:"login"`
	Password string `json:"-"`
}

type LoginAnswer struct {
	Id  int64
	Key string
}

type PublicMsg struct {
	Text   string `json:"text"`
	Author *User  `json:"user"`
}

var (
	db = pg.Connect(&pg.Options{
		User:     "postgres",
		Password: "",
		Database: "postgres",
		Addr:     "db:5432",
	})
	msgs          []PublicMsg
	logginedUsers = make(map[int64]string)
)

func register(c *gem.Context) {
	c.Request.ParseForm()
	user := &User{
		Nickname: c.Request.PostFormValue("nickname"),
		Login:    c.Request.PostFormValue("login"),
		Password: c.Request.PostFormValue("password"),
	}
	count, err := db.Model(&User{}).Where("nickname = ?", user.Nickname).WhereOr("login = ?", user.Login).Count()
	if err != nil {
		c.JSON(200, "Failed")
		panic(err)
	}
	if count != 0 {
		c.JSON(200, "nickname or login exists")
		return
	}
	err = db.Insert(user)
	if err != nil {
		c.JSON(200, "Failed")
		panic(err)
	}
	c.JSON(200, "Ok")
}

func login(c *gem.Context) {
	loginName := c.Request.PostFormValue("login")
	password := c.Request.PostFormValue("password")
	user := &User{
		Login:    loginName,
		Password: password,
	}
	count, err := db.Model(user).Where("login = ?", loginName).Where("password = ?", password).Count()
	if err != nil {
		c.JSON(200, "Failed")
		panic(err)
	}
	hash := sha256.New()
	rand.Seed(rand.Int63())
	hash.Write([]byte(strconv.Itoa(rand.Int())))
	hashString := hex.EncodeToString(hash.Sum(nil))
	db.Model(user).Where("login = ?", loginName).Where("password = ?", password).Select()
	la := &LoginAnswer{
		Id:  user.Id,
		Key: hashString,
	}
	logginedUsers[user.Id] = hashString
	if count == 1 {
		c.JSON(200, la)
	} else {
		c.JSON(200, "Failed")
	}
}

func showPublicMessages(c *gem.Context) {
	if len(msgs) > 100 {
		c.JSON(200, msgs[len(msgs)-100:])
	} else {
		c.JSON(200, msgs)
	}
}

func sendMsg(c *gem.Context) {
	c.ParseForm()
	hash := c.PostFormValue("hash")
	id, err := strconv.Atoi(c.PostFormValue("id"))
	if err != nil {
		panic(err)
	}
	if logginedUsers[int64(id)] != hash {
		c.JSON(200, "Failed")
		return
	}
	text := c.PostFormValue("text")
	var user = &User{Id: int64(id)}
	db.Model(user).Select()
	msg := new(PublicMsg)
	msg.Text = text
	msg.Author = user
	msgs = append(msgs, *msg)
	c.JSON(200, "Ok")
}

func showAllUsers(c *gem.Context) {
	var users []User
	err := db.Model(&users).Select()
	if err != nil {
		panic(err)
	}
	c.JSON(200, users)
}

func changeNickname(c *gem.Context) {
	c.ParseForm()
	newNick := c.PostFormValue("new_nickname")
	hash := c.PostFormValue("hash")
	id, err := strconv.Atoi(c.PostFormValue("id"))
	if logginedUsers[int64(id)] != hash {
		c.JSON(200, "Failed")
		return
	}
	if err != nil {
		panic(err)
	}
	user := &User{Id: int64(id)}
	db.Model(user).Set("nickname = ?", newNick).Where("id = ?", user.Id).Update()
}

func main() {
	defer db.Close()
	err := createSchema(db)
	if err != nil {
		panic(err)
	}

	fmt.Println("start")
	srv := gem.New(":8080")

	// Create router.
	router := gem.NewRouter()
	// Register handler
	router.POST("/register", register)
	router.POST("/login", login)
	router.GET("/users", showAllUsers)
	router.GET("/msgs", showPublicMessages)
	router.POST("/msgs", sendMsg)
	router.POST("/change_nickname", changeNickname)

	// Start server.
	log.Println(srv.ListenAndServe(router.Handler()))
}

func createSchema(db *pg.DB) error {
	for _, model := range []interface{}{(*User)(nil), (*PublicMsg)(nil)} {
		err := db.CreateTable(model, &orm.CreateTableOptions{
			Temp: true,
		})
		if err != nil {
			return err
		}
	}
	return nil
}
